# Conpass - Desafio

Challenge front-end Conpass

## Instalação

* Baixar o repositório
* yarn install (ou npm)

## Ambiente de desenvolvimento

* cd challeng-conpass
* yarn start (ou npm start)

## Ambiente de produção

* yarn build (ou npm build)

