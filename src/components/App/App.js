import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import Header from "../Header"
import ListUsers from "../ListUsers"
import Register from "../Register"


class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path='/' component={ListUsers} />
          <Route path='/register' component={Register} />

        </Switch>
      </div>
    );
  }
}

export default App;
