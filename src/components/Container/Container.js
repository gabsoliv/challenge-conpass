import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from '@material-ui/core/styles';

export const styles = theme => ({
  root: {
    maxWidth: 1180,
    padding: `0 16px`,
    margin: "0 auto"
  }
});

function Container({ classes, className, component: Component, ...other }) {
  return (
    <Component className={classNames(classes.root, className)} {...other} />
  );
}

Container.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  component: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
};

Container.defaultProps = {
  component: "div"
};

export default withStyles(styles, { name: "MuiContainer" })(Container);
