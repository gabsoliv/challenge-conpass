import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import logo from "./logo.png";

class Header extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
        <AppBar className={classes.root} position="static" color="default">
          <div className={classes.logo}>
            <img src={logo} alt="conpass" />
          </div>
        </AppBar>
      </div>
    );
  }
}

const styles = theme => ({
  root : {
    marginBottom: theme.spacing.unit * 5,
    background: '#fff'
  },
  logo: {
    padding: theme.spacing.unit * 3,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
});


export default withStyles(styles)(Header);
