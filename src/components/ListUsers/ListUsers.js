import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Container from "../Container";
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Warning from '@material-ui/icons/Warning';

class ListUsers extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      usersData: JSON.parse(localStorage.getItem('persist:root'))
    }
  }

  createTable = () => {
    const { classes } = this.props;
    const { usersData } = this.state
    const users = usersData && JSON.parse(usersData.todos);
    const reverseList = usersData && users.reverse()
    let index = 0
    let table = []

    if (users) {
      for (index; index < users.length; index++) {
        let firstCharName = users[index].value.nameField[0]
        let firstCharLast = users[index].value.lastNameField[0]

        table.push(
          <TableRow key={index}>
            <TableCell component="th" scope="row">
              <div className={classes.root2}>
                {!users[index].value.imageField ? (
                  <div className={classes.charName}>{firstCharName}{firstCharLast}</div>
                ) : (
                    <div className={classes.maskImage}>
                      <div className={classes.image} style={{ background: `url(${users[index].value.imageField}) no-repeat` }}></div>
                    </div>
                  )}

                <div>
                  {users[index].value.nameField + ' ' + users[index].value.lastNameField}
                </div>
              </div>
            </TableCell>
            <TableCell >
              {users[index].value.dateField}
            </TableCell>
          </TableRow>
        )
      }
    } 

    if (users === null || users.length === 0) {
      return (
        <TableRow key={index}>
          <TableCell className={classes.noUser}>
            <Warning className={classes.warningIcon} />
            <Typography variant="title"> Não há  nenhum usuário cadastrado!</Typography>
          </TableCell>
        </TableRow>)
    }
    return table
  }

  render() {
    const { classes } = this.props;

    return (
      <Container>
        <div className={classes.root}>
          <Typography variant="display2">Users</Typography>
          <Button component={Link} to="/Register" variant="contained" color="primary" >Add new User</Button>
        </div>

        <Paper className={classes.paper}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>
                  <Typography className={classes.tableTitle}>Full name</Typography>
                </TableCell>
                <TableCell>
                  <Typography className={classes.tableTitle}>Created at</Typography>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.createTable()}
            </TableBody>

          </Table>

        </Paper>
      </Container>
    );
  }
}

const styles = theme => ({
  root: {
    display: "flex",
    justifyContent: "space-between"
  },
  root2: {
    display: "flex",
    alignItems: 'center',
    '& div': {
      marginRight: theme.spacing.unit
    }
  },
  paper: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  noUser: {
    paddingTop: theme.spacing.unit * 5,
    paddingBottom: theme.spacing.unit * 5,
    display: 'flex',
  },
  warningIcon: {
    marginRight: theme.spacing.unit,
    color: '#FFD600',
  },
  tableTitle: {
    fontWeight: 'bold',
    color: "#12c1c7",
  },
  maskImage: {
    borderRadius: '100%',
    maxWidth: '50px',
    height: '50px',
    overflow: 'hidden',
    background: '#d8d8d8'
  },
  image: {
    width: '50px',
    height: '50px',
    backgroundPosition: 'center!important',
    backgroundSize: 'contain!important',
  },
  charName: {
    width: '50px',
    height: '50px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '100%',
    background: '#d8d8d8',
    color: '#6a7272',
    fontWeight: 'bold',
    textTransform: 'uppercase'
  }
});

export default withStyles(styles)(ListUsers);
