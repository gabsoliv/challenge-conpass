import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withRouter } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import FileBase64 from 'react-file-base64';
import * as EmailValidator from 'email-validator';

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as TodoActions from "../../store/actions/todos";
import PersonalDataHeader from "./PersonalDataHeader";
import ImageHeader from "./ImageHeader";

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return 0;
    case 1:
      return 1;
    default:
      return "Uknown stepIndex";
  }
}

class TodoList extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  state = {
    firstName: "",
    lastName: "",
    companyName: "",
    email: "",
    password: "",
    repeatPassword: "",
    activeStep: 0,
    imageProfile: "",
    files: [],
    validationEmail: 1,
    validationPassword: 1
  };

  handleNext = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep + 1,
    });
  };

  handleBack = () => {
    const { activeStep } = this.state;
    this.setState({
      activeStep: activeStep - 1,
    });
  };

  getFiles(files) {
    this.setState({
      files: files,
      imageProfile: files.base64
    });
  }

  emailValidation = () => {
    const isValidated = EmailValidator.validate(this.state.email)
    this.setState({ validationEmail: isValidated })
  }

  passwordValidation = () => {
    const { password, repeatPassword } = this.state;

    if (password !== repeatPassword) {
      this.setState({ validationPassword: false })
    } else {
      this.setState({ validationPassword: true })
    }
  }

  handleSubmit = event => {
    event.preventDefault();
    const { firstName, lastName, companyName, email, password, repeatPassword, imageProfile } = this.state;
    var date = new Date();
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
    const registerDate = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + ", " + date.getHours() + ":" + date.getMinutes()
    const inputObje = { nameField: firstName, lastNameField: lastName, companyNameField: companyName, emailField: email, passwordField: password, repeatPasswordField: repeatPassword, dateField: registerDate, imageField: imageProfile }
    this.props.addTodo(inputObje);
    this.setState({
      firstName: "",
      lastName: "",
      companyName: "",
      email: "",
      password: "",
      repeatPassword: "",
    });

    this.props.history.push("/");
    this.forceUpdate()
  };

  render() {
    const { classes } = this.props;
    const { firstName, lastName, companyName, email, password, repeatPassword, activeStep, imageProfile, validationEmail, validationPassword } = this.state;
    const isEnabled =
      firstName.length > 0 &&
      lastName.length > 0 &&
      companyName.length > 0 &&
      validationEmail &&
      email.length > 0 &&
      password.length > 0 &&
      repeatPassword.length > 0 &&
      validationPassword;

    return (
      <div className="App">
        <form onSubmit={this.handleSubmit}>
          {getStepContent(activeStep) === 0 &&
            <Grid container spacing={24}>
              <PersonalDataHeader />

              <Grid item xs={12} md={6}>
                <TextField
                  name="firstName"
                  label="First Name"
                  placeholder="First Name"
                  type="text"
                  onChange={e => this.setState({ firstName: e.target.value })}
                  value={firstName}
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  name="lastName"
                  label="Last Name"
                  placeholder="Last Name"
                  type="text"
                  onChange={e => this.setState({ lastName: e.target.value })}
                  value={lastName}
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  name="companyName"
                  label="Company Name"
                  placeholder="Company Name"
                  type="text"
                  onChange={e => this.setState({ companyName: e.target.value })}
                  value={companyName}
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  name="email"
                  label="Email"
                  placeholder="Email"
                  type="email"
                  onChange={e => this.setState({ email: e.target.value })}
                  onBlur={this.emailValidation.bind(this)}
                  value={email}
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
                {!validationEmail && (
                  <Typography color="error">Fill the field correctly</Typography>
                )}
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  name="password"
                  label="Password"
                  placeholder="Password"
                  type="password"
                  onChange={e => this.setState({ password: e.target.value })}
                  value={password}
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
              </Grid>

              <Grid item xs={12} md={6}>
                <TextField
                  name="repeatPassword"
                  label="Repeat Password"
                  placeholder="Repeat Password"
                  type="password"
                  onChange={e => this.setState({ repeatPassword: e.target.value })}
                  onBlur={this.passwordValidation.bind(this)}
                  value={repeatPassword}
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: classes.bootstrapInput,
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
                {!validationPassword && (
                  <Typography color="error">Password don't match</Typography>
                )}
              </Grid>

              <Grid item xs={12}>
                <Button variant="contained" className={classes.stepButton} onClick={this.handleNext} disabled={!isEnabled}>
                  next step >
                </Button>
              </Grid>
            </Grid>
          }

          {getStepContent(activeStep) === 1 && (
            <Grid container justify="center" spacing={24}>
              <ImageHeader />

              <Grid item xs={4}>
                <div className={classes.uploadButton}>
                  {!imageProfile ? (
                    <button className={classes.btn}></button>
                  ) : (
                      <div className={classes.maskProfile}>
                        <div style={{ background: `url(${imageProfile}) no-repeat` }} className={classes.btnImage} alt="" />
                      </div>
                    )}
                  <FileBase64
                    multiple={false}
                    onDone={this.getFiles.bind(this)} />
                </div>
              </Grid>
              <Grid item xs={9}>
                <Button onClick={this.handleBack}>back step</Button>
                <Button className={classes.stepButton} type="submit" variant="contained">Finish ></Button>
              </Grid>
            </Grid>
          )}
        </form>
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    display: "flex",
    justifyContent: "space-between"
  },
  uploadButton: {
    position: 'relative',
    overflow: 'hidden',
    display: 'inline-block',
    marginBottom: theme.spacing.unit * 4,
    '& input[type=file]': {
      fontSize: '100px',
      position: 'absolute',
      left: '0',
      top: '0',
      opacity: '0',
    }
  },
  btn: {
    border: 'none',
    color: '#636363',
    backgroundColor: '#d8d8d8',
    padding: '8px 20px',
    fontSize: '20px',
    fontWeight: 'bold',
    width: '130px',
    height: '130px',
    borderRadius: '100%',
    '&:before': {
      content: "'click to upload your profile image'",
      position: 'absolute',
      fontSize: '11px',
      left: '12px',
      top: '40%',
      width: '105px'
    }
  },
  btnImage: {
    border: 'none',
    color: '#636363',
    backgroundColor: '#d8d8d8',
    padding: '8px 20px',
    fontSize: '20px',
    fontWeight: 'bold',
    width: '130px',
    height: '130px',
    backgroundPosition: 'center!important',
    backgroundSize: 'contain!important',
    '&:before': {
      content: "'Editar'",
      position: 'absolute',
      fontSize: '11px',
      left: '0',
      top: '77%',
      width: '85%',
      borderRadius: '10px',
      textAlign: 'center',
      color: '#ffffff',
      backgroundColor: '#30cf94',
      padding: '7px'
    }
  },
  maskProfile: {
    borderRadius: '100%',
    background: '#fff',
    maxWidth: '130px',
    height: '130px'
  },
  stepButton: {
    backgroundColor: '#30cf94',
    color: '#ffffff'
  },
  bootstrapRoot: {
    padding: 0,
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  bootstrapInput: {
    borderRadius: 4,
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 12px',
    width: 'calc(100% - 24px)',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
  bootstrapFormLabel: {
    fontSize: 18,
  },
});

const mapStateToProps = state => ({
  users: state.users
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(TodoActions, dispatch);
withStyles(styles)

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TodoList)));