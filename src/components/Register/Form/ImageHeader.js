import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

class ImageHeader extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <Grid item xs={12}>
        <div className={classes.root} >
          <Typography variant="display2">Profile Photo</Typography>
        </div>
      </Grid>
    );
  }
}

const styles = theme => ({
  root: {
    display: "flex",
    justifyContent: "space-around"
  },
});


export default withStyles(styles)(ImageHeader);