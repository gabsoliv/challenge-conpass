import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Typography from '@material-ui/core/Typography';

class PersonalDataHeader extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  state = {
    activeStep: 0,
  };

  render() {
    const { classes } = this.props;
    const { activeStep } = this.state;

    return (
      <Grid item xs={12}>
        <div className={classes.root} >
          <Typography variant="display2">Register</Typography>
          <div>
            <Stepper
              className={classes.steppers}
              activeStep={activeStep}
              alternativeLabel
            >
              <Step className={classes.stepsItem}>
                <StepLabel>Personal Data</StepLabel>
              </Step>
              <Step>
                <StepLabel>Profile Photo</StepLabel>
              </Step>
            </Stepper>
          </div>
        </div>
      </Grid>
    );
  }
}

const styles = theme => ({
  root: {
    display: "flex",
    justifyContent: "space-between"
  },
  steppers: {
    background: "#f9f9f9",
  }
});


export default withStyles(styles)(PersonalDataHeader);