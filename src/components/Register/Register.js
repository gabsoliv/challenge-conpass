import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Container from "../Container";
import Form from "./Form"

class Register extends Component {

  render() {
    return (
      <Container>
        <Grid container justify="center" spacing={24}>
          <Grid item xs={5}>
              <Form />
          </Grid>
        </Grid>
      </Container>
    );
  }
}




export default Register;
