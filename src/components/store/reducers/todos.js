export default function todos(state = [], action) {
    switch (action.type) {
        case "ADD_TODO":
            return [...state, { value: action.text}];
        default:
            return state;
    }
}