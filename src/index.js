import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom'
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Provider } from "react-redux";
import { store, persistor } from "./components/store";
import { PersistGate } from 'redux-persist/integration/react';

ReactDOM.render((
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <MuiThemeProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </MuiThemeProvider>
    </PersistGate>
  </Provider>
), document.getElementById('root'))
registerServiceWorker();
